#ifndef SUBSCRIPTION_H
#define SUBSCRIPTION_H

#include "constants.h"


struct precipitation
{
    double day;
    double month;
    double quantity;
    char type[MAX_STRING_SIZE];
};

#endif#pragma once
