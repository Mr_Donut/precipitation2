#include <iostream>
#include <iomanip>

using namespace std;

#include "subscription.h"
#include "file_reader.h"
#include "constants.h"
#include "filter.h"
#include "processing.h"

void output(precipitation* subscription)
{
    /********** ����� ����  **********/
                     // ����� �����
    cout << "����: ";
    cout << setw(2) << setfill('0') << subscription->day << ' ';
    // ����� ������
    cout << "�����: ";
    cout << setw(2) << setfill('0') << subscription->month;
    cout << '\n';
    /********** ����� ���-�� ������� **********/
    cout << "���-�� �������: ";
    cout << setw(4) << setfill('0') << subscription->quantity << ' ';
    cout << '\n';
    /********** ����� ��� ������� **********/
    cout << "��� �������: ";
    cout << setw(4) << setfill('0') << subscription->type << ' ';
    cout << '\n';
    cout << '\n';

}

int main()
{
    setlocale(LC_ALL, "Russian");
    cout << "Laboratory work #8. GIT\n";
    cout << "Variant #3. Precipitation\n";
    cout << "Author: Artem Goncharov\n";
    cout << "Group: 12\n";
    precipitation* subscriptions[MAX_FILE_ROWS_COUNT];
    int size;
    try
    {
        read("data.txt", subscriptions, size);
        cout << "***** Precipitation *****\n\n";
        for (int i = 0; i < size; i++)
        {
            output(subscriptions[i]);
        }
        bool (*check_function)(precipitation*) = NULL; 
                                                        
        cout << "\n �������:\n";
        cout << "1)����� ��� ���, � ������� ��� �����. \n";
        cout << "2)����� ��� ���, � ������� ����� ������� ��� ������ 1,5. \n";
        cout << "3)������� ����� ���������� ������� �� ��������� ����� (�������� �������������) \n";
        cout << "\n ������� � ";
        int item;
        cin >> item;
        cout << '\n';
        switch (item)
        {
        case 1:
            check_function = all_the_days_it_rained; //       
            cout << "���, � ������� ��� �����.\n\n";
            break;
        case 2: {
            check_function = volume_of_precipitation_was_less_than_1_5; //       
            cout << "���, � ������� ����� ������� ��� ������ 1,5.\n\n";
            break;
        }
        default:
            throw "  ";
        }
        if (check_function)
        {
            int new_size;
            precipitation** filtered = filter(subscriptions, size, check_function, new_size);
            for (int i = 0; i < new_size; i++)
            {
                output(filtered[i]);
            }
            delete[] filtered;
        }
        for (int i = 0; i < size; i++)
        {
            delete subscriptions[i];
        }
    }
    catch (const char* error)
    {
        cout << error << '\n';
    }
    return 0;
}
