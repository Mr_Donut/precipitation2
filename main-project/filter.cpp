#include "filter.h"
#include <cstring>
#include <iostream>

precipitation** filter(precipitation* array[], int size, bool (*check)(precipitation* element), int& result_size)
{
	precipitation** result = new precipitation * [size];
	result_size = 0;
	for (int i = 0; i < size; i++)
	{
		if (check(array[i]))
		{
			result[result_size++] = array[i];
		}
	}
	return result;
}

bool all_the_days_it_rained(precipitation* element)
{
	return element->type[0] =='r';
}

bool volume_of_precipitation_was_less_than_1_5(precipitation* element)
{
	return element->quantity <1.5;
}
