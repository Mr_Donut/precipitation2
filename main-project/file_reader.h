#ifndef FILE_READER_H
#define FILE_READER_H

#include "subscription.h"

void read(const char* file_name, precipitation* array[], int& size);

#endif#pragma once
