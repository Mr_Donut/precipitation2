#ifndef FILTER_H
#define FILTER_H

#include "subscription.h"

precipitation** filter(precipitation* array[], int size, bool (*check)(precipitation* element), int& result_size);




bool all_the_days_it_rained(precipitation* element);




bool volume_of_precipitation_was_less_than_1_5(precipitation* element);



#endif
#pragma once
